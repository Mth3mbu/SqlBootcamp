CREATE PROCEDURE [spGetTask]
@taskID INT
AS
SELECT  [Users].[Firstname] AS 'Created By',
		[Users].[Surname],
		[Tasks].[Name] AS 'Task Name',
		[Tasks].[Notes],
		[Tasks].[CreationDate],
		[Priorities].[Priority],
		[Assignee].[Firstname] As 'Assignee'
FROM [Tasks] 
		INNER JOIN [Users] 
		ON [Tasks].[UserId]=[Users].[ID]
		INNER JOIN [Priorities] 
		ON [Tasks].[PriorityId]=[Priorities].[ID]
		INNER JOIN [Users] AS [Assignee]
		ON [Tasks].[AssigneeId]=[Assignee].[ID]
WHERE [Tasks].[ID]=@taskID

GO

