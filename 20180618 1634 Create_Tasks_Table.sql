CREATE TABLE [Tasks](
 [ID] INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
 [Name] [VARCHAR](50) NOT NULL,
 [Description] [VARCHAR] (150)NOT NULL,
 [CompletionDate] [DATETIME2] NULL,
 [CreationDate] [DATETIME2] NULL,
 [ModificationDate] [DATETIME2] NULL,
 [UserId] INT  NOT NULL,
 [UpdatedBy] INT NULL
)