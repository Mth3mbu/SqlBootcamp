CREATE PROCEDURE [spGetTaskByAssignee]
@assigneeID INT
AS
SELECT [Tasks].[Name],
	 [Tasks].[Notes],
	 [Priorities].[Priority],
	 CONCAT([Users].[Firstname],' ',[Users].[Surname],'(',FORMAT([Tasks].[CreationDate],'yyyy-mm-dd'),')') AS 'Created By',CONCAT([Users].[Firstname],' ',[Users].[Surname],'(',FORMAT([Tasks].[ModificationDate],'yyyy-mm-dd'),')') AS 'Last Updated By'
FROM [Tasks]
	 INNER JOIN [Priority] 
	 ON [Tasks].[PriorityId]=[Priorities].[ID]
	 INNER JOIN [Users]
	 ON [Tasks].[UserId]=[Users].[ID]
	 INNER JOIN [Users] AS [Assignee]
	 ON [Tasks].[AssigneeId]=[Assignee].[ID]
WHERE [Tasks].[AssigneeId]=@assigneeID
GO


