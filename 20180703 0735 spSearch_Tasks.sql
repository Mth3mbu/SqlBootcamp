CREATE PROCEDURE [spSearchTask]
@priority VARCHAR (50),
@taskName VARCHAR (50),
@assignee VARCHAR (50),
@notes VARCHAR (50)
AS
SELECT  [Tasks].[Name],
		[Tasks].[Notes],
		[Priorities].[Priority],
		CONCAT([Users].[Firstname],'(',FORMAT([Tasks].[CreationDate],'yyyy-MM-dd'),')') As 'Assignee'
FROM    [Tasks]
		INNER JOIN [Users]
		ON [Tasks].[AssigneeId]=[Users].[ID]
		INNER JOIN [Priorities] 
		ON [Tasks].[PriorityId]=[Priorities].[ID] 
		WHERE [Priority] LIKE '%'+@priority +'%'
		OR [Name] LIKE '%'+@taskName+'%'
		OR [Users].[Firstname] LIKE '%'+ @assignee +'%'
		OR [Notes] LIKE '%'+@notes+'%'
GO


