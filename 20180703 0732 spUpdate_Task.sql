CREATE PROCEDURE [spUpdateTask]
@asigneeID INT,
@taskID INT,
@updatedBy INT,
@Notes VARCHAR (500),
@Name VARCHAR (50)
AS
UPDATE Tasks 
SET [Name]=@Name,
    [Notes]=@Notes,
	[ModificationDate]=SYSDATETIME(),
	[UpdatedBy]=@updatedBy,
	[AssigneeId]=@asigneeID
WHERE [ID]=@taskID
GO
