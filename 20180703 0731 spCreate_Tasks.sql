CREATE PROCEDURE [spCreateTasks]
@Name VARCHAR (50),
@Notes VARCHAR (50),
@UserId INT,
@PriorityId INT,
@asigneeId INT
AS
INSERT INTO [Tasks]
 ([Name],
  [Notes],
  [CreationDate],
  [UserId],
  [PriorityId],
  [AssigneeId])
 VALUES (@Name,
  @Notes,
  SYSDATETIME(),
  @UserId,
  @PriorityId,
  @asigneeId)
GO