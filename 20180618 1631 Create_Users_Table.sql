CREATE TABLE [Users](
 [ID] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
 [Firstname] [VARCHAR](50) NOT NULL,
 [Surname] [VARCHAR] (150) NOT NULL,
 [Email] [VARCHAR](150) NULL,
 [Active] [BIT]  NULL,
 [CreationDate] [DATETIME2] NULL,
 [ModificationDate] [DATETIME2] NULL
)