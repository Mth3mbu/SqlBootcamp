CREATE TABLE [dbo].[DBVersion](
       [DatabaseVersion] [varchar](50) NOT NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[ScriptLog](
       [ScriptName] [varchar](250) NOT NULL,
       [ScriptDate] [smalldatetime] NOT NULL,
       [RunDate] [datetime] NULL,
       [DBVersion] [varchar](50) NOT NULL,
       [Comments] [varchar](150) NOT NULL
) ON [PRIMARY]
